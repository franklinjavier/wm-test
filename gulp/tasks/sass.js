var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sync = require('browser-sync'),
    util = require('../util');

gulp.task('sass', function () {  

    gulp.src('./app/styles/*.scss')

        // Parseia os arquivos scss
        .pipe(sass({
            includePaths: ['scss']
        }))

        // Reporta erros de compilacao
        .on('error', util.handleErrors)

        // Especifica path de destino
        .pipe( gulp.dest('./dist/css') )

        // Reload no browser-sync
        .pipe( sync.reload({ stream: true }) );
});

