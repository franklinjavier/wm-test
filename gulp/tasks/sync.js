var gulp = require('gulp'),
    sync = require('browser-sync');

gulp.task('sync', ['scripts'], function() {

    sync.init(['./app/index.html'], {

        // Serve conteudo a partir desse path
        server: {
            baseDir: './dist'
        },

        // Nao abre o browser automaticamente
        open: false,

        // Oculta box do canto
        notify: false,

        // Sincroniza eventos
        ghostMode: {
            clicks: true,
            location: true,
            forms: true,
            scroll: true
        }
    });

});
