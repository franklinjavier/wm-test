'use strict';

require('angular');
require('ui-router');
require('angular-local-storage');
require('./ngLocale');

var config = require('./config'),
    req = [
        'ui.router',
        'LocalStorageModule',
        'ngLocale'
    ];
    
angular.module( config.appName, req )
    .constant('CONFIG', config)
    .config(require('./states'))
    .factory('cartFactory', require('./cart/CartFactory'))
    .factory('profileFactory', require('./profile/ProfileFactory'))
    .factory('addressFactory', require('./address/AddressFactory'))
    .controller('CartController', require('./cart/CartController'))
;
