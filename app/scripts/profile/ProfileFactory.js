'use strict';

module.exports = function() {

    var Profile = {};

    /*
     * Retorna dados do usuario
     *
     * Poderia consumir uma API e retornar 
     * os dados da mesma forma
     */
    Profile.get = function() {
        return {
            name: 'Franklin Javier',
            gender: 'Masculino',
            cpf: '111.111.111-11',
            birthdate: '05/01/1986',
            tel: '(11) 99999-9999',
            email: 'franklinjalves@gmail.com'
        };
    };
    
    return Profile;

};

