'use strict';

module.exports = function() {

    var Cart = {};

    /*
     * Retorna os itens do carrinho
     *
     * Poderia consumir uma API e retornar 
     * os dados da mesma forma
     */
    Cart.get = function() {

	    return [{
	        image: 'http://placehold.it/100x100',
	        name: 'iPhone 6 16Gb',
	        price: 4000.99,
	        description: 'Apple iPhone 16GB Cinza Espacial MG3A2BZ/A',
	        qtt: '1'
        },{
	        image: 'http://placehold.it/100x100',
	        name: 'iPhone 6 8Gb',
	        price: 3199.00,
	        description: 'Apple iPhone 8GB Ouro MG3D2BZ/A',
	        qtt: '1'
        }];

    };

    return Cart;

};
