var fs = require('fs'),
    util = require('./util'),
    tasks = fs.readdirSync('./gulp/tasks/', util.onlyScripts);

// Requisita todas as tasks
tasks.forEach(function( task ) {
	require('./tasks/' + task);
}); 
