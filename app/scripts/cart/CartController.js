'use strict';

module.exports = function( $scope, cartFactory, localStorageService ) {
	
    $scope.pageTitle = 'Meu carrinho';

    // Popula os dados do localStorage ou
    // requisita da factory
    $scope.items = localStorageService.get('items') || cartFactory.get();

    $scope.total = calculateTotal;

    function calculateTotal() {

        var total = 0;

        angular.forEach( $scope.items, function( v, k ) {

            // Total geral
            total += v.qtt * v.price;

            // Persiste no localStorage
            localStorageService.set('items', $scope.items);

        });

        return total;
    }

};


