/*global angular */

'use strict';

describe('Unit: CartController', function() {

    var $rootScope, $scope, $controller, ctrl, titlePage; 

    beforeEach(function() {
        // instantiate the app module
        angular.mock.module('checkout');

        beforeEach(inject(function(_$rootScope_, _$controller_){
            $rootScope = _$rootScope_;
            $scope = $rootScope.$new();
            $controller = _$controller_;
            ctrl = $controller('CartController', {'$rootScope' : $rootScope, '$scope': $scope});
        }));

    });

    it('titulo deve ser igual', function() {
        expect($scope.pageTitle).toEqual('Meu carrinho');
    });

    it('controller deve existimport', function() {
        expect($controller).toBeDefined();
    });

    it('testando retorno da colecao de itens', function() {
        expect($scope.items).toBe(true);
        expect($scope.items.length).toBe(1000);
    });

});
