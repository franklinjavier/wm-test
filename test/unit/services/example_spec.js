/*global angular */

'use strict';

describe('Unit: cartFactory', function() {

  var service;

  beforeEach(function() {
    // instantiate the app module
    angular.mock.module('checkout');

    // mock the service
    angular.mock.inject(function(cartFactory) {
      service = cartFactory;
    });
  });

  it('should exist', function() {
    expect(service).toBeDefined();
  });

});
