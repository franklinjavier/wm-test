## Sintetizando os motivos das tecnologia empregadas na aplicação

### Karma e Jasmine 
Escolhi Karma e Jamine por uma série de beneficios

- Não é engessado à nenhum browser, framework ou plataforma. 
- Possibilita executar os testes onde o JavaScript roda (browsers, servers, smartphones, etc)
- Documentação excelente! 


### GulpJS
- Escolhi GulpJS frente aos outros task runners por sua simplicidade de implementação e por ser mais rápido, pois utiliza Streams (parte importante do NodeJS).

### AngularJS
- O principal ponto é o two-way data binding que abstrai as etapas de manipulação do DOM, deixando o programador mais focado na interação das entidades.
- Outro ponto crucial é a melhoria na produtividade e manutenção (escreva menos e faça mais).
- Angular tem a ideia de modulos, que ajuda a modularizar e tornar o código em libs reutilizáveis.
- Desenvolvimento mais declarativo, novas tags e diretivas extendidas, que podem ser reutilizáveis em qualquer parte da aplicação (DRY dont repeat yourself).
- Organização do framework proporciona uma alta testabilidade

## Como rodar

 ```
 npm install
 gulp
 ```

 Testes
 ```
 npm test
 ```