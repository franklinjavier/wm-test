'use strict';

module.exports = function( $scope, profileFactory, localStorageService ) {
	
    $scope.pageTitle = 'Perfil';

    // Popula os dados do localStorage ou
    // requisita da factory
    $scope.profile = localStorageService.get('profile') || profileFactory.get();
    
    // Persiste no localStorage
    $scope.$watch('profile', function () {
        localStorageService.set('profile', $scope.profile);
    }, true);

};
