'use strict';

module.exports = function( $locationProvider, $stateProvider ) {
  
    $locationProvider
        .html5Mode( false );

    $stateProvider
        .state('cart', {
            url: '/cart',
            controller: require('./cart/CartController'),
            templateUrl: 'views/cart.html'
        })
        .state('profile', {
            url: '/profile',
            controller: require('./profile/ProfileController'),
            templateUrl: 'views/profile.html'
        })
        .state('address', {
            url: '/address',
            controller: require('./address/AddressController'),
            templateUrl: 'views/address.html'
        })
        .state('feedback', {
            url: '/feedback',
            controller: require('./feedback/FeedbackController'),
            templateUrl: 'views/feedback.html'
        });
};
