'use strict';

/*
 * Mascara para formato moeda
 */
module.exports.formatMoney = function() {

    return function( input ) {

        var num = input.toString().split('.');
        var decimals = 2;

        num[0] = num[0].split('').reverse().join('').replace(/(\d{3})(?=\d)/g, '$1'+ ('.')).split('').reverse().join('');

        if (decimals) {
            num[1] = num[1] ? num[1].slice(0, decimals) : '';
            if (num[1].length < decimals) { 
                num[1] += new Array(decimals - num[1].length + 1).join('0');
            }
        } else if (num[1]) {
            num.pop();
        }

        return 'R$ ' + num.join(',');

    };
};

