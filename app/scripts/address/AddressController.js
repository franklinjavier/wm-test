'use strict';

module.exports = function( $scope, addressFactory, localStorageService ) {
	
    $scope.pageTitle = 'Endereço';

    // Popula os dados do localStorage ou
    // requisita da factory
    $scope.address = localStorageService.get('address') || addressFactory.get();
    
    // Persiste no localStorage
    $scope.$watch('address', function () {
        localStorageService.set('address', $scope.address);
    }, true);

};
