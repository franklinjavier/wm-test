var gulp = require('gulp'),
    sync = require('browser-sync');
//runSequence = require('run-sequence');

gulp.task('default', ['lint', 'sync', 'sass', 'views'], function() {

    gulp.watch('./app/scripts/*.js', ['lint', 'scripts']);
    gulp.watch('./app/styles/*.scss', ['sass']);
    gulp.watch(['./app/index.html', './app/views/**/*.html'], ['views']);

});

