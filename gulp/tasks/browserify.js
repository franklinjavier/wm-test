var gulp = require('gulp'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    uglify = require('gulp-uglify'),
    streamify = require('gulp-streamify'),
    watchify = require('watchify'),
    util = require('../util'),
    sync = require('browser-sync'),
    concat = require('gulp-concat'),
    ngAnnotate = require('browserify-ngannotate')

gulp.task('scripts', function() {

    var bundler = watchify('./app/scripts/main');

    //bundler.transform('debowerify');
    bundler.transform( ngAnnotate );
    bundler.on('update', rebundle);
    
    function rebundle() {

        return bundler

        // Ativa source maps
        .bundle({ debug: true })

        // Reporta erros de compilacao
        .on('error', util.handleErrors)

        // vinyl-source-stream para manter o 
        // stream compativel com gulp 
        .pipe( source('main.js') )
        //.pipe( streamify( uglify() ) )

        // Destino
        .pipe( gulp.dest('./dist/js') )

        // Reload no browser-sync
        .pipe( sync.reload({ stream: true, once: true }) );

    }
    
    return rebundle();

});
