/*global angular */

'use strict';

describe('Unit: CONFIG', function() {

  var constants;

  beforeEach(function() {

    // instantiate the app module
    angular.mock.module('checkout');

    // mock the directive
    angular.mock.inject(function(CONFIG) {
      constants = CONFIG;
    });
  });

  it('should exist', function() {
    expect(constants).toBeDefined();
  });

  it('should have an application name', function() {
    expect(constants.appName).toEqual('checkout');
  });

});
