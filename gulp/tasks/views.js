var gulp = require('gulp'),
    sync = require('browser-sync');

gulp.task('views', function() {

    gulp.src('./app/index.html')
        .pipe( gulp.dest('./dist/') )
        .pipe( sync.reload({ stream: true }) );

    gulp.src('./app/views/**/*')
        .pipe( gulp.dest('./dist/views/') )
        .pipe( sync.reload({ stream: true }) );
});

