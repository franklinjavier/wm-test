var path = require('path'),
    notify = require('gulp-notify');

module.exports = {

    // Apenas arquivos js
    // Evita incluir acidentalmente arquivos ocultos
    scriptFilter: function( name ) { 
        return /(\.(js)$)/i.test( path.extname( name ) );
    },

    handleErrors: function() {

        var args = Array.prototype.slice.call( arguments );

        // Sistema de notificacao com gulp-notify
        notify.onError({
            title: 'Compile Error',
            message: '<%= error.message %>'
        }).apply( this, args );

    }

};

