'use strict';

module.exports = function() {

    var Address = {};

    /*
     * Retorna dados do endereco do usuario
     *
     * Poderia consumir uma API e retornar 
     * os dados da mesma forma
     */
    Address.get = function() {
        return {
            name: 'Franklin Javier',
            postalcode: '01310-100',
            address: 'Av Paulista',
            number: '620',
            neighborhood: 'Bela Vista',
            city: 'São Paulo'
        };
    };
    
    return Address;

};
